/**
 * This is the main module for the Delivery Reporter front-end Web application
 * @module DeliveryReporter
 */
var DeliveryReporter = DeliveryReporter || {}
DeliveryReporter.List = {
  
  /**
   * This module is the List component of the GUI
   * @class List
   */
  
  /**
   * Initializes the List Module
   * @method init
   * @param {Element} root The root DOM element for the List Module
   * @param {Element} listRoot The DOM root element for the List module's list
   * @param {Element} btnCreate The DOM element for the List modules's create button
   * @param {String} itemStyle The classname for this module's list item styles
   * @param {String} detailsStyle The classname for this module's list item detail styles
   * @param {String} buttonStyle The classname for this module's button styles
   * @returns {Object} An Object with the List module's public API
   */
	init ( {root, listRoot, btnCreate, itemStyle, detailsStyle, buttonStyle} ) {

		// Public Methods

      /**
       * Returns a reference to the module's root element
       * @method getRoot
       * @returns {Element}
       */
      const getRoot = () => {
        return _root
      }

      /**
       * Returns a report object with the id that is passed in as an argument
       * @method getReportById
       * @param {Number} id The id of the report object
       * @param {Function} callback A function that will be called after the report is found
       */
      const getReportById = ( {id, callback} ) => {
        for ( let i = 0; i < _reports.length; ++i ) {
          if (_reports[i].report_id == id) {
            return callback( _reports[i] )
          } 
        }
        return callback( null )
      }

      /**
       * Binds a function to the click event on the create button
       * @method setCreateListener
       * @param {Function} createListener A function that will listen for a click event on the create button
       */
      const setCreateListener = (createListener) => {
        if (_createListener !== null) {
          _btnCreate.removeEventListener( 'click', _createListener )
        }
        _createListener = createListener
        _btnCreate.addEventListener( 'click', _createListener )
      }

      /**
       * Binds a function to the click event on the delete button
       * @method setDeleteListener
       * @param {Function} deleteListener A function that will listen for a click event on the delete button
       */
      const setDeleteListener = (deleteListener) => {
        if (_deleteListener !== null) {
          _btnDelete.removeEventListener( 'click', _deleteListener )
        }
        _deleteListener = deleteListener
        _btnDelete.addEventListener( 'click', _deleteListener )
      }

      /**
       * Binds a function to the click event on the update button
       * @method setUpdateListener
       * @param {Function} setUpdateListener A function that will listen for a click event on the update button
       */
      const setUpdateListener = (updateListener) => {
        if (_updateListener !== null) {
          _btnUpdate.removeEventListener( 'click', _updateListener )
        }
        _updateListener = updateListener
        _btnUpdate.addEventListener( 'click', _updateListener )
      }

      /**
       * Deletes all of the delivery report data stored in this module
       * @method clearReports
       */
      const clearReports = () => {
        _reports = null
        if (_expanded) {
          _expanded.removeAttribute( 'expanded' )
          _expanded.childNodes[1].removeChild( _btnDelete )
          _expanded.childNodes[1].removeChild( _btnUpdate )
          _expanded.childNodes[1].removeChild( _detailsBox )
          _expanded = null
        }
      }

      /**
       * Creates DOM Elements for each delivery report if there is data
       * @method createList
       * @param {Array} data An array of report objects
       * @returns {Promise} This Promise will fill the List module with data
       */
      const createList = (data) => {
          return new Promise( (resolve, reject) => {
            if (data) {
              _reports = data
              adjustPool( () => {
                  fillSlots( resolve )
              } )
            } else {
              reject( false )
            }
        })
      }

		// Private Properties
      const _root           = root
      const _listRoot       = listRoot
      const _btnCreate      = btnCreate
      const _btnDelete      = document.createElement( 'button' )
      const _btnUpdate      = document.createElement( 'button' )
      const _detailsBox     = document.createElement( 'div' )
      const _itemStyle      = itemStyle
      const _detailsStyle   = detailsStyle
      const _buttonStyle    = buttonStyle
      var   _createListener = null,
      _deleteListener       = null,
      _updateListener       = null,
      _reports              = null,
      _reportItemPool       = [],
      _expanded             = null
      
		// Private Methods

			// Adjusts the report item elements to match 
			const adjustPool = (callback) => {

        if (_reportItemPool.length === _reports.length) {}
        else if (_reportItemPool.length > _reports.length) {
          while (_reportItemPool.length > _reports.length) {
            let removed = _reportItemPool.pop()
            _listRoot.removeChild( removed.root )
          }
        } else {
          while (_reportItemPool.length < _reports.length) {
            // Create DOM Elements
            let newPoolItem      = document.createElement( 'div' ),
            newTopRow            = document.createElement( 'div' ),
            newBottomRow         = document.createElement( 'div' ),
            newDate              = document.createElement( 'p' ),
            newDay               = document.createElement( 'p' ),
            newStoreLocation     = document.createElement( 'p' ),
            newHoursWorked       = document.createElement( 'p' ),
            newTotalTips         = document.createElement( 'p' ),
            newGasMoney          = document.createElement( 'p' ),
            newProfit            = document.createElement( 'p' )
            newPoolItem.classList.add( _itemStyle ),
            newPoolItem.addEventListener( 'click', expandItem )

            // Append DOM Elements
            newPoolItem.appendChild( newTopRow )
            newPoolItem.appendChild( newBottomRow )
            newTopRow.appendChild( newDate )
            newTopRow.appendChild( newDay )
            newTopRow.appendChild( newStoreLocation )
            //newTopRow.appendChild( newHoursWorked )
            //newTopRow.appendChild ( newTotalTips )
            //newTopRow.appendChild( newGasMoney )
            //newTopRow.appendChild( newProfit )
            _listRoot.appendChild( newPoolItem )
            
            // Create Item Slot
            _reportItemPool.push( {
              root         : newPoolItem,
              bottomRow    : newBottomRow,
              date         : newDate,
              day          : newDay,
              storeLocation: newStoreLocation,
              hoursWorked  : newHoursWorked,
              totalTips    : newTotalTips,
              gasMoney     : newGasMoney,
              profit       : newProfit
            } )
          }
        }
        callback()
      }

      // Fills all item slots with delivery report data
      const fillSlots = (callback) => {
        for( let i = 0; i < _reports.length; ++i ) {
          _reportItemPool[i].root.setAttribute( 'tag', _reports[i].report_id )
          _reportItemPool[i].id                        = _reports[i].report_id
          _reportItemPool[i].date.textContent          = _reports[i].date.substring(0, _reports[i].date.indexOf('T'))
          _reportItemPool[i].day.textContent           = _reports[i].day_of_week
          _reportItemPool[i].storeLocation.textContent = _reports[i].store_location
          _reportItemPool[i].hoursWorked               = _reports[i].hours_worked
          _reportItemPool[i].totalTips                 = `$${_reports[i].total_tips}`
          _reportItemPool[i].gasMoney                  = `$${_reports[i].gas_money}`
          _reportItemPool[i].profit                    = `$${_reports[i].profit}`
        }
      }

      // Binds a function to each report item element that will expanded it when clicked on
      const expandItem = (e) => {
        e.stopPropagation()
        var item = e.target
        while ( !item.getAttribute( 'tag' ) ) {
          item = item.parentNode
        }
        const tag = item.getAttribute( 'tag' )

        if (item === _expanded) {
          _expanded = null
          item.childNodes[1].removeChild( _btnDelete )
          item.childNodes[1].removeChild( _btnUpdate )
          item.childNodes[1].removeChild( _detailsBox )
          return item.removeAttribute( 'expanded' )
        } 

        if (_expanded !== null) {
          _expanded.removeAttribute( 'expanded' )
          _expanded.childNodes[1].removeChild( _btnDelete )
          _expanded.childNodes[1].removeChild( _btnUpdate )
          _expanded.childNodes[1].removeChild( _detailsBox )
        } 

        for (let i = 0; i < _reportItemPool.length; ++i) {
          if (_reportItemPool[i].id ==  tag) {
            console.log('foundit')
            _detailsBox.childNodes[1].childNodes[0].innerHTML = _reportItemPool[i].hoursWorked
            _detailsBox.childNodes[1].childNodes[1].innerHTML = _reportItemPool[i].totalTips
            _detailsBox.childNodes[1].childNodes[2].innerHTML = _reportItemPool[i].gasMoney
            _detailsBox.childNodes[1].childNodes[3].innerHTML = _reportItemPool[i].profit
            break
          }
        }
        
        _btnDelete.setAttribute( 'tag', tag )
        _btnUpdate.setAttribute( 'tag', tag )
        item.childNodes[1].appendChild( _btnUpdate )
        item.childNodes[1].appendChild( _btnDelete )
        item.childNodes[1].appendChild( _detailsBox )
        _expanded = item
        item.setAttribute( 'expanded', 'true' )
      }

    // Initialize the delte and update buttons
    _detailsBox.classList.add( _detailsStyle )

    let detailsHeaderRow = document.createElement( 'p' )
    detailsHeaderRow.appendChild( document.createElement( 'p' ) )
    detailsHeaderRow.lastChild.appendChild( document.createTextNode( 'Hours Worked' ) )
    detailsHeaderRow.appendChild( document.createElement( 'p' ) )
    detailsHeaderRow.lastChild.appendChild( document.createTextNode( 'Total Tips' ) )
    detailsHeaderRow.appendChild( document.createElement( 'p' ) )
    detailsHeaderRow.lastChild.appendChild( document.createTextNode( 'Gas Money' ) )
    detailsHeaderRow.appendChild( document.createElement( 'p' ) )
    detailsHeaderRow.lastChild.appendChild( document.createTextNode( 'Total Profit' ) )
    
    let detailsDataRow = document.createElement( 'p' )
    detailsDataRow.appendChild( document.createElement( 'p' ) )
    detailsDataRow.appendChild( document.createElement( 'p' ) )
    detailsDataRow.appendChild( document.createElement( 'p' ) )
    detailsDataRow.appendChild( document.createElement( 'p' ) )

    _detailsBox.appendChild( detailsHeaderRow )
    _detailsBox.appendChild( detailsDataRow )

    _btnDelete.appendChild( document.createTextNode( 'DELETE' ) )
    _btnDelete.classList.add( _buttonStyle )
    _btnUpdate.appendChild( document.createTextNode( 'UPDATE' ) )
    _btnUpdate.classList.add( _buttonStyle )

    // Return the Public API
    return {
      getRoot          : getRoot,
      getReportById    : getReportById,
      clearReports     : clearReports,
      createList       : createList,
      setCreateListener: setCreateListener,
      setDeleteListener: setDeleteListener,
      setUpdateListener: setUpdateListener
    }
  }
}