/**
 * This is the main module for the Delivery Reporter front-end Web application
 * @module DeliveryReporter
 */
var DeliveryReporter = DeliveryReporter || {}
DeliveryReporter.Navbar = {
  
  /**
   * This module is the Navigation component of the GUI layout
   * @class Navbar
   */

  /**
   * Initializes the Navbar Module
   * @method init
   * @param {Element} root The root DOM element for the Navbar Module
   * @param {Element} btnLogout The DOM element for the Navbar's logout button
   * @returns {Object} An Object with the Navbar module's public API
   */
	init ( {root, btnLogout} ) {

		// Public Methods

      /**
       * Returns a reference to the module's root element
       * @method getRoot
       * @returns {Element}
       */
      const getRoot = () => {
        return _root
      }

      /**
       * Binds a function to the click event on the submit button
       * @method setLogoutListener
       * @param {Function} listener A function that will listen for a click event on the logout button
       */
      const setLogoutListener = (listener) => {
        if (_btnLogout) {
          if (logoutListener != null) {
            _btnLogout.removeEventListener( 'click', logoutListener )
          }
          logoutListener = listener
          _btnLogout.addEventListener( 'click', logoutListener )
        }
      }

		// Private Properties
      const _root = root
      const _btnLogout = btnLogout
      var logoutListener = null
      
		// Private Methods

			// Checks whether or not the AJAX dependency is present
			const priv = () => {

      }


    // Return the Public API
    return {
      getRoot:           getRoot,
      setLogoutListener: setLogoutListener
    }
  }
}