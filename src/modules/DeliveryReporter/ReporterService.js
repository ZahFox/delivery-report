/**
 * This is the main module for the Delivery Reporter front-end Web application
 * @module DeliveryReporter
 */
var DeliveryReporter = DeliveryReporter || {}
DeliveryReporter.ReporterService = {

  /**
   * This module is a service that uses the AJAX module to handle communication with the REST API
   * @class ReporterService
   */

  /**
   * Initializes the ReporterService Module
   * @method init
   * @param {Object} AJAX A reference to the AJAX module that this module will use
   * @returns {Object} An Object with the ReporterService module's public API
   */
	init ( {AJAX} ) {

		// Public Methods

      /**
       * Preforms a login attempt and then executes a callback function
       * @method login
       * @param {String} username The username value
       * @param {String} password The password value
       * @param {Function} callback A function that will be called after the login attempt
       */
			const login = ( {username, password, callback} ) => {
        ajax.send( {
          url:         `${baseURL}login`,
          method:      'POST',
          contentType: 'application/json',
          body:        JSON.stringify( {username: username, password: password} )
        } )
          .then( ( {success, token} ) => {
            if (success === true) {
              saveToken( token )
              callback( true )
            } else {
              callback( false )
            }
          } )
      }

      /**
       * Logs the user out by removing the token from local storage
       * @method logout
       * @param {Function} callback A function that will be called after the logout
       */
      const logout = (callback) => {
        removeToken()
        callback()
      }
      
      /**
       * Sends a POST request to create a new report
       * @method createReport
       * @param {Number} id The id of the report object
       * @param {Object} data The data for the report object
       * @param {Function} callback A function that will be called after the HTTP request
       */
      const createReport = ( {data, callback} ) => {
         ajax.send( {
          url:         `${baseURL}reports`,
          body:        JSON.stringify( data ),
          method:      'POST',
          headers: {
            'Content-Type':   'application/json',
            'x-access-token': getToken()
          }
         } )
           .then( ( {affectedRows, insertId} ) => {
            (affectedRows === 1) ? callback( insertId ) : callback( false )
           } )
      }

      /**
       * Sends a GET request to retrieve all reports
       * @method getReports
       * @param {Function} callback A function that will be called after the HTTP request
       */
      const getReports = (callback) => {
        ajax.send( {
          url:         `${baseURL}reports`,
          method:      'GET',
          headers: {
            'Content-Type':   'application/json',
            'x-access-token': getToken()
          }
         } )
           .then( (res) => {
            Array.isArray( res ) ? callback( res ) : callback(false)
           } )
      }

      /**
       * Sends a GET request to retrieve an individual report
       * @method getReport
       * @param {Number} id The id of the report object
       * @param {Function} callback A function that will be called after the HTTP request
       */
      const getReport = ( {id, callback} ) => {
        ajax.send( {
          url:         `${baseURL}reports/${id}`,
          method:      'GET',
          headers: {
            'Content-Type':   'application/json',
            'x-access-token': getToken()
          }
         } )
           .then( (res) => {
            (typeof( res ) === 'object') ? callback( res ) : callback( false )
           } )
      }

      /**
       * Sends a PUT request to update a report
       * @method updateReport
       * @param {Number} id The id of the report object
       * @param {Object} data The data for the report object
       * @param {Function} callback A function that will be called after the HTTP request
       */
      const updateReport = ( {id, data, callback} ) => {
        ajax.send( {
          url:         `${baseURL}reports/${id}`,
          body:        JSON.stringify( data ),
          method:      'PUT',
          headers: {
            'Content-Type':   'application/json',
            'x-access-token': getToken()
          }
         } )
           .then( ( {affectedRows, changedRows} ) => {
            (affectedRows === 1) ? callback( changedRows ) : callback( false )
           } )
      }

      /**
       * Sends a DELETE request to delete a report
       * @method deleteReport
       * @param {Number} id The id of the report object
       * @param {Function} callback A function that will be called after the HTTP request
       */
      const deleteReport = ( {id, callback} ) => {
        ajax.send( {
          url:         `${baseURL}reports/${id}`,
          method:      'DELETE',
          headers: {
            'Content-Type':   'application/json',
            'x-access-token': getToken()
          }
         } )
          .then( ( {affectedRows} ) => {
            (affectedRows === 1) ? callback( affectedRows ) : callback( false )
          } )
      }

      /**
       * Checks to see if there is a token or if the current token is valid
       * @method checkToken
       * @param {Function} callback A function that will be called after the HTTP request
       */
      const checkToken = (callback) => {
        let token = getToken()
        if (token) {
          ajax.send( {
            url:         `${baseURL}validate`,
            method:      'GET',
            headers: {
              'Content-Type':   'application/json',
              'x-access-token':  token
            }
           } )
             .then( ( res ) => {
               if (res && res.valid) return callback( true )
                  return callback( false )
             } )
             .catch ( ( res ) => { 
               return callback( false )
               } )
        } else {
          return callback( false )
        }
      }

		// Private Properties
      const ajax = AJAX
      const baseURL = 'https://okami.services/'

		// Private Methods

			// Checks whether or not the AJAX dependency is present
			const validateInit = () => {
        return ajax ? true : false
      }
      
      const saveToken = (token) => {
        window.localStorage.setItem( 'token', token )
      }

      const getToken = () => {
        return window.localStorage.getItem( 'token' )
      }

      const removeToken = () => {
        window.localStorage.removeItem( 'token' )
      }

    // Return the Public API if it was successfully initialized
    if (validateInit) {
      return {
        login:        login,
        logout:       logout,
        createReport: createReport,
        getReports:   getReports,
        getReport:    getReport,
        updateReport: updateReport,
        deleteReport: deleteReport,
        checkToken:   checkToken
      }
    } else {
      return null
    }
	}
}
