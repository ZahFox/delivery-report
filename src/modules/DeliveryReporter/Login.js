/**
 * This is the main module for the Delivery Reporter front-end Web application
 * @module DeliveryReporter
 * @main The namespace for the Delivery Reporter Web Application
 */
var DeliveryReporter = DeliveryReporter || {}
DeliveryReporter.Login = {
  
  /**
   * This module is the Login component of the GUI
   * @class Login
   */

  /**
   * Initializes the Login Module
   * @method init
   * @param {Element} root The root DOM element for the Login Module
   * @returns {Object} An Object with the Login module's public API
   */
	init ( {root} ) {

		// Public Methods

      /**
       * Returns the form's current data
       * @method getData
       * @returns {Object} { username, password }
       */
			const getData = () => {
        return { username: _txtUsername.value, password: _txtPassword.value }
      }

      /**
       * Returns a reference to the module's root element
       * @method getRoot
       * @returns {Element}
       */
      const getRoot = () => {
        return _root
      }

      /**
       * Clears the input values from the login form
       * @method clearForm
       */
      const clearForm = () => {
        _txtUsername.value = ''
        _txtPassword.value = ''
      }

      /**
       * Binds a function to the click event on the submit button
       * @method setSubmitListener
       * @param {Function} listener A function that will listen for a click event on the submit button
       */
      const setSubmitListener = (listener) => {
        if (submitListener != null) {
          _btnSubmit.removeEventListener( 'click', submitListener )
        }
        submitListener = listener
        _btnSubmit.addEventListener( 'click', submitListener )
      }

		// Private Properties
      const _root = root
      const _txtUsername = root.querySelector( 'input[type=text]' )
      const _txtPassword = root.querySelector( 'input[type=password]' )
      const _btnSubmit = root.querySelector( 'button' )
      var submitListener = null

		// Private Methods

			// Checks whether or not the AJAX dependency is present
			const priv = () => {

      }


    // Return the Public API
    return {
      getData:           getData,
      getRoot:           getRoot,
      clearForm:         clearForm,
      setSubmitListener: setSubmitListener
    }
  }
}