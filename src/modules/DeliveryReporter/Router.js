/**
 * This is the main module for the Delivery Reporter front-end Web application
 * @module DeliveryReporter
 */
var DeliveryReporter = DeliveryReporter || {}
DeliveryReporter.Router = {

  /**
   * This module is the Router component of the GUI.
   * It is responsible for controlling which GUI components are active.
   * @class Router
   */
  
  /**
   * Initializes the Router Module
   * @method init
   * @param {Object} routes An object that holds all of the routing information for the application that uses this module
   * @param {String} begin The name for the route that will be transitioned to first when the application starts
   * @returns {Object} An Object with the Router module's public API
   */
	init ( {routes, begin} ) {

		// Public Methods

      /**
       * Transitions from the current route to the next route
       * @method transition
       * @param {String} name The name of the next route
       */
			const transition = (name) => {
        // First, see if the next route exists
        let nextRoute = _routes.find( (route) => route.name === name )
        
        // If it does, preform a transition or beforeEach
        if (nextRoute) {
          _beforeEach()

          if (currentRoute !== null) {
            currentRoute.exit()
          }
          currentRoute = nextRoute
          currentRoute.enter()
        } else {
          throw `Error: Route '${name}' does not exist!`
        }
      }

      /**
       * Binds a function to be called before each transition
       * @method setBeforeEach
       * @param {Function} beforeEach The function to be called before each transition 
       */
      const setBeforeEach = (beforeEach) => {
        if (beforeEach && typeof(beforeEach) === 'function') {
          _beforeEach = beforeEach
        }
      }

		// Private Properties
      const _routes = routes
      var currentRoute = null
      var _beforeEach = () => {}
      
      
		// Private Methods

			// Checks whether or not the AJAX dependency is present
			const priv = () => {

      }

    // Check for a begin argument
      if (begin) {
        transition( begin )
      }

    // Return the Public API if it was successfully initialized
    return {
      transition:    transition,
      setBeforeEach: setBeforeEach
    }
  }
}