/**
 * This is the main module for the Delivery Reporter front-end Web application
 * @module DeliveryReporter
 */
var DeliveryReporter = DeliveryReporter || {}
DeliveryReporter.Details = {
  
  /**
   * This module is the Details component of the GUI.
   * @class Details
   */
  
  /**
   * Initializes the Details Module
   * @method init
   * @param {Element} root The root DOM element for the Details Module
   * @param {Element} btnSubmit The DOM element for the Details modules's submit button
   * @param {Element} btnCancel The DOM element for the Details modules's cancel button
   * @param {Element} txtDate The DOM element for the Details modules's date text box
   * @param {Element} optDay The DOM element for the Details modules's day of the week select box  
   * @param {Element} optStore The DOM element for the Details modules's store location select box
   * @param {Element} txtHours The DOM element for the Details modules's hours worked text box 
   * @param {Element} txtTips The DOM element for the Details modules's tip money text box
   * @param {Element} txtGas The DOM element for the Details modules's gas money text box
   * @returns {Object} An Object with the Details module's public API
   */
	init ( {root, btnSubmit, btnCancel, txtDate, optDay, optStore, txtHours, txtTips, txtGas} ) {

		// Public Methods

      /**
       * Returns a reference to the module's root element
       * @method getRoot
       * @returns {Element}
       */
      const getRoot = () => {
        return _root
      }

      /**
       * Returns the form's current data
       * @method getData
       * @returns {Object} { day, day_of_week, store_location, hours_worked, total_tips, gas_money, profit }
       */
			const getData = () => {
        let tips   = roundTwo( _txtTips.value )
        let gas    = roundTwo( _txtGas.value )
        let profit = roundTwo( (tips - gas) )
        return {
          date          : _txtDate.value,
          day_of_week   : _optDay.value,
          store_location: _optStore.value,
          hours_worked  : roundTwo( _txtHours.value ),
          total_tips    : tips,
          gas_money     : gas,
          profit        : profit
        }
      }

      /**
       * Fills the form with data
       * @method getData
       * @param {Object} { day, day_of_week, store_location, hours_worked, total_tips, gas_money, profit }
       */
      const setData = ( {date, day, store, hours, tips, gas, callback} ) => {
        _txtDate.value  = new Date( date ).toDateInputValue( true )
        _optDay.value   = day
        _optStore.value = store
        _txtHours.value = hours
        _txtTips.value  = tips
        _txtGas.value   = gas
        callback()
      }

      /**
       * Binds a function to the click event on the submit button
       * @method setSubmitListener
       * @param {Function} submitListener A function that will listen for a click event on the submit button
       */
      const setSubmitListener = (submitListener) => {
        if (_submitListener != null) {
          _btnSubmit.removeEventListener( 'click', _submitListener )
        }
        _submitListener = submitListener
        _btnSubmit.addEventListener( 'click', _submitListener )
      }

      /**
       * Binds a function to the click event on the cancel button
       * @method setCancelListener
       * @param {Function} cancelListener A function that will listen for a click event on the cancel button
       */
      const setCancelListener = (cancelListener) => {
        if (_cancelListener != null) {
          _btnCancel.removeEventListener( 'click', _cancelListener )
        }
        _cancelListener = cancelListener
        _btnCancel.addEventListener( 'click', _cancelListener )
      }

      /**
       * Clears the input values from the details form
       * @method clearForm
       */
      const clearForm = () => {
        let newDate    = new Date()
        _optDay.value  = _days[ newDate.getDay() ]
        _txtDate.value = newDate.toDateInputValue()
        _optStore.value = _stores[0]
        _txtHours.value = ''
        _txtTips.value = ''
        _txtGas.value = ''
      }

		// Private Properties
      const _root           = root
      const _btnSubmit      = btnSubmit
      const _btnCancel      = btnCancel
      const _txtDate        = txtDate
      const _optDay         = optDay
      const _optStore       = optStore
      const _txtHours       = txtHours
      const _txtTips        = txtTips
      const _txtGas         = txtGas
      var   _submitListener = null
      var   _cancelListener = null
      const _days           = ['SUNDAY','MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY','SATURDAY']
      const _stores         = ['PEARL ST.', 'LOSEY BLVD.']
      
		// Private Methods

      // Fills a select element with option elements
      const fillOptions = (parent, values) => {
        for ( let value of  values ) {
          let newOption = document.createElement( 'option' )
          newOption.value = value
          newOption.appendChild( document.createTextNode( value ) )
          parent.appendChild(newOption)
        }
      }

      // Rounds string numbers to two digits
      const roundTwo = (n) => {
        n = parseFloat( n )
        var negative = false
        if(n < 0) {
          negative = true
          n = n * -1
        }
        var multiplicator = Math.pow( 10, 2 )
        n = parseFloat( ( n * multiplicator ).toFixed( 11 ) )
        n = (Math.round( n ) / multiplicator).toFixed( 2 )
        if(negative) {    
            n = (n * -1).toFixed( 2 )
        }
        return parseFloat( n ) | 0
      }
        

    // Initialize the dynamic form elements (days of the week, and store locations)
      fillOptions(_optDay,  _days)
      fillOptions(_optStore, _stores)
    
    // Initialize the date input to default to the current date
      Date.prototype.toDateInputValue = (function(offset) {
          var local = new Date( this )
          if (offset) local.setMinutes( this.getMinutes() )
          else local.setMinutes( this.getMinutes() - this.getTimezoneOffset() )

          return local.toJSON().slice( 0, 10 )
      })
      clearForm()

    // Return the Public API if it was successfully initialized
    return {
      getRoot          : getRoot,
      getData          : getData,
      setData          : setData,
      setSubmitListener: setSubmitListener,
      setCancelListener: setCancelListener,
      clearForm        : clearForm
    }
  }
}