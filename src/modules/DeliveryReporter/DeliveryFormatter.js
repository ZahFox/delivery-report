const DeliveryFormatter = {
	
	init (params) {

		// Public Methods

			// Formats raw delivery objects for use with with the REST API
			const formatDeliveryObjects = (data) => {
				return data.map( (obj) => { 
					return {
						date:           `${obj.year}-${monthMap[obj.month]}-${obj.day}`,
						day_of_week:    obj.dayOfWeek,
						store_location: obj.storeLocation,
						hours_worked:   parseFloat( obj.hoursWorked ),
						gas_money:      parseCash( obj.gasMoney ),
						profit:         parseCash( obj.profit ),
						total_tips:     parseCash( obj.totalTips )
					}
				})
			}

		// Private Properties
			const monthMap = {JANUARY:   1, FEBRUARY:  2, MARCH:     3,
				                APRIL:     4, MAY:       5, JUNE:      6,
												JULY:      7, AUGUST:    8, SEPTEMBER: 9,
												OCTOBER:  10, NOVEMBER: 11, DECEMBER: 12}

		// Private Methods

			// Parses a strings like: '$(8.00)' or '$10.24', into number variables
			const parseCash = (data) => {
				let num = (res = data.charAt(1) === '(' ? ('-' + data.slice(2)) : ( data.slice(1)))
				let decimal = num.indexOf('.')
				if (decimal !== -1) {
					return (  parseFloat(num.substring(0, decimal)) + parseFloat(num.substring(decimal))  )
				} else {
					return parseFloat(num) | 0
				}
			}

		// Return the Public API
		return {
			formatDeliveryObjects: formatDeliveryObjects
		}
	}
}
