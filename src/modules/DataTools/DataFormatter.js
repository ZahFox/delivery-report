const DataFormatter = {

  init (_params) {

    // Public Methods

    // This method will take properly formatted CSV data and return it as an array of objects
    const csvToObjects = (_data, _lowerCaseKeys, _delimiter) => {
      // Split each row of the CSV string into an array
      const csvRows = _data.trim().split(_delimiter || /\r\n?|\n/)
      
      // The first row will be the keys for each object's properties
      const keys = _lowerCaseKeys ? csvRows[0].split(",")
        .map( (_key) => {
          return camelize(_key.toLowerCase())
        }) : csvRows[0].split(",")
      
      // Return the rest of the rows as objects with a property for each key
      return csvRows.slice(1)
        .map( (_row) => _row.split(","))
        .map( (_row) => {
          let obj = {}
          for(let i = 0; i < keys.length; ++i) {
              obj[keys[i]] = _row[i].trim()
          }
          return obj
        })
    }


    // This method will take an object or array of objects and return them as a JSON string
    const objectsToJSON = (_data) => {
      if(typeof(_data) == "object")
          return JSON.stringify(_data)
    }


    // This method will create an XML element with a required name, and optional attributes or text
    const createXMLElement = (_name, _attributes, _text) => {
      let element = document.createElementNS(null, _name)

      if(_attributes) {
          for(let i = 0; i < _attributes.length; ++i) {
              element.setAttribute(_attributes[i].name,  _attributes[i].value)
          }
      }
      
      if(_text) {
          let text = document.createTextNode(_text)
          element.appendChild(text)
      }  
      return element;
    }


		// Private Methods

		// This method converts a string into camelCase e.g. the best ever => theBestEver
		const camelize = (_string) => {
			return _string.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(_match, _index) {
				if (+_match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
				return _index == 0 ? _match.toLowerCase() : _match.toUpperCase();
			});
		}

    // Return the Public API
    return {
    	csvToObjects: csvToObjects,
    	objectsToJSON: objectsToJSON,
    	createXMLElement: createXMLElement
    }
  }
}