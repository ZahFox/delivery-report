const DataLoader = {

  CSV_Loader: {
    init (_params) {

			var callback = _params.callback || null
			var rootElement = _params.rootElement || null
			var label = null
			var labelText = _params.labelText || null
			var uploadButton = null

			// Set Up the Upload Label
			label = document.createElement('h4');
			label.textContent = 'Upload a CSV file';
			rootElement.appendChild(label)

			// Set Up the Upload Input
			uploadButton = document.createElement('input')
			uploadButton.setAttribute('type', 'file')
			uploadButton.setAttribute('accept', '.csv')
			rootElement.appendChild(uploadButton)


			// Public Methods
			const setCallback = (_callback) => {
				callback = _callback
				uploadButton.addEventListener('change', handleCSV)
			}


			// Private Methods
			const handleCSV = (_e) => {
				// get the data from the selected file and pass it to the callback function
				let file = _e.target.files[0];
				let reader = new FileReader();
				reader.addEventListener('load', (_evt) => {
				let data = _evt.target.result;
				callback(data);
			});
			
			reader.readAsText(file); 
		}

			// Return the Public API
			return {
				setCallback: setCallback
			}
		}
  }
}