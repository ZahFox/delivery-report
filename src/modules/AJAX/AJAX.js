/**
 * This module is used for sending JavaScript HTTP requests from a Web browser
 * @module AJAX
 */

/**
 * This is AJAX module
 * @class AJAX
 */
const AJAX = {
  
  /**
   * Initializes the AJAX Module
   * @method init
   * @returns {Params} An Object with the initialization parameters for this module
   */
	init (params) {

		// Public Methods

			// Sends the HTTP request
			const send = (params) => new Promise( (resolve, reject) => {
        // Make sure params are set and are an object
        if (!params && typeof params !== 'object') reject( 'No parameters passed' )
        
        // Create new HTTP instance
        const http = new XMLHttpRequest()
        
        // Ensure URL
        if (!params.url) reject( 'Invalid request, must include URL parameter' )
        
        // Set and ensure method
        const method = params.method || 'GET'
        if (methods.indexOf( method.toUpperCase() ) === -1) reject( 'Invalid method parameter' )
        
        // Open http req
        http.open( method, params.url, true )
        
        // Set headers
        if (params.headers) {
          Object.keys( params.headers ).forEach( (key) => http.setRequestHeader( key, params.headers[key] ) )
        }
        
        // Set accept header
        if (params.accept) http.setRequestHeader( 'Accept', params.accept )
    
        // Set content-type header
        if (params.contentType) http.setRequestHeader( 'Content-Type', params.contentType )
        
        // Determine if there is/should be a body
        const body = [ 'POST', 'PUT', 'PATCH' ].indexOf( method ) >= 0
          ? params.body || null // Use body param or null
          : null // No body to send
        
        // Send
        http.send( body )
        
        // Listen and respond
        http.addEventListener( 'readystatechange', () => {
          if (http.readyState === 4) {
            // Success
            if (http.status === 200) resolve( JSON.parse( http.responseText ) )
            // Error
            reject( 'HTTP Error: ' + http.status )
          }
        })
      })

		// Private Properties
      const methods = [ 'GET', 'POST', 'PUT', 'PATCH', 'DELETE' ]

		// Return the Public API
		return {
			send: send
		}
	}
}