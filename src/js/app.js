window.addEventListener( 'load', () => {

  // DOM References
  const root = document.querySelector( 'body' )
  const wrapper = document.querySelector( '[name=wrapper]' )
  const uploadRoot = document.getElementById( 'upload-root' )
  const loader = document.getElementById( 'loader' )

  // Configure the GUI Content Modules
  const login = DeliveryReporter.Login.init( {root: document.getElementById( 'login-root' )} )
  const list = DeliveryReporter.List.init( {
    root        : document.getElementById( 'list-root' ),
    listRoot    : document.querySelector( '.report-list' ),
    btnCreate   : document.querySelector( '.list-top > button' ),
    itemStyle   : 'report-list-item',
    detailsStyle: 'report-item-details',
    buttonStyle : 'report-list-button'
  } )
  const details = DeliveryReporter.Details.init( {
    root     : document.getElementById( 'details-root' ),
    btnSubmit: document.getElementById( 'details-button' ),
    btnCancel: document.getElementById( 'details-cancel-button' ),
    txtDate  : document.querySelector( '[name=date]' ),
    optDay   : document.querySelector( '[name=day_of_week]' ),
    optStore : document.querySelector( '[name=store_location]' ),
    txtHours : document.querySelector( '[name=hours_worked]' ),
    txtTips  : document.querySelector( '[name=total_tips]' ),
    txtGas   : document.querySelector( '[name=gas_money]' )
  } )
  
  // Configure the GUI Layout Module
  const navbar = DeliveryReporter.Navbar.init( { 
    root: document.getElementById( 'nav-root' ),
    btnLogout: document.querySelector( '.nav-logout button' )
  } )

  // Initialize Utility Modules
  const service = DeliveryReporter.ReporterService.init( {AJAX: AJAX.init()} )


  // These are not needed for this version

  // const csvLoader = DataLoader.CSV_Loader.init( {
  //   rootElement: uploadRoot,
  //   labelText: 'Upload File'
  // } )

  // const dataFormatter = DataFormatter.init()
  // const deliveryFormatter = DeliveryFormatter.init()





  // Configure the GUI Router Module
  const router = DeliveryReporter.Router.init( { routes: [

    {
      name:  'Login',
      enter () {
        enableDialogLayout( login.getRoot() )
      },
      exit () {
        login.clearForm()
        disableDialogLayout( login.getRoot() )
      },
      root: login.getRoot()
    },

    {
      name:  'List',
      enter () {
        service.getReports( (data) => {
          list.createList( data ).then(
            () => {
              enableFullLayout( list.getRoot() )
            }
          ).catch( enableFullLayout( list.getRoot() ) )
        } )
      },
      exit () {
        list.clearReports()
        disableFullLayout( list.getRoot() )
      },
      root: list.getRoot()
    },

    {
      name: 'Details',
      enter () {
        enableFullLayout( details.getRoot() )
      },
      exit () {
        details.clearForm()
        disableFullLayout( details.getRoot() )
      }
    }

  ] } )
  router.setBeforeEach( () => {

  })

  // Bind Functions to GUI Component Event Listeners
  navbar.setLogoutListener( (e) => {
    e.stopPropagation()
    service.logout( () => router.transition( 'Login' ) )
  } )

  list.setCreateListener( (e) => {
    e.stopPropagation()
    details.setSubmitListener( (evt) => {
      evt.stopPropagation()
      service.createReport( {
        data: details.getData(),
        callback () {
          router.transition( 'List' )
        }
      } )
    })
    router.transition( 'Details' )
  })

  list.setUpdateListener( (e) => {
    e.stopPropagation()
    const id = e.target.getAttribute( 'tag' )
    // find the report data
    list.getReportById( {id: id, callback ( report ) {
      // fill details form with the report data
      details.setData( {
        date : report.date,
        day  : report.day_of_week,
        store: report.store_location,
        hours: report.hours_worked,
        tips : report.total_tips,
        gas  : report.gas_money,
        callback () {
          // make the form's submit button PUT the updated data to API
          details.setSubmitListener( (evt) => {
            evt.stopPropagation()
            service.updateReport( {
              id: id,
              data: details.getData(),
              callback () {
                router.transition( 'List' )
              }
            } )
          } )
          // finally, transition to the the details form
          router.transition( 'Details' )
        }
      } )
    } } )
  } )

  list.setDeleteListener( (e) => {
    e.stopPropagation()
    service.deleteReport( {id: e.target.getAttribute( 'tag' ),
      callback () {
        service.getReports( (data) => {
          list.clearReports()
          list.createList( data ).then(
            () => {
              
            }
          ).catch( () => {} )
        } )
      }
    } )
  } )

  details.setCancelListener( (e) => {
    e.stopPropagation()
    router.transition( 'List' )
  })

  login.setSubmitListener( (e) => {
    e.preventDefault()
    e.stopPropagation()
    let {username, password} = login.getData()
    service.login( {
      username: username.trim(),
      password: password.trim(),
      callback: (res) => {
        if (res === true) {
          router.transition( 'List' )
        }
      }
     } )
   }
  )


  // Validate Client's Token
  service.checkToken( (valid) => {
    valid ? router.transition( 'List' ) : router.transition( 'Login' )
  })


  // App Specific Layout Functions
  function enableFullLayout (content) {
    wrapper.removeAttribute( 'curtain' )
    root.removeAttribute( 'dialogLayout' )
    root.setAttribute( 'fullLayout', true )
    navbar.getRoot().removeAttribute( 'hidden' )
    content.removeAttribute( 'hidden' )
    loader.setAttribute( 'hidden', 'true' )
  }

  function disableFullLayout (content)  {
    loader.removeAttribute( 'hidden' )
    content.setAttribute( 'hidden', 'true' )
  }

  function enableDialogLayout (content) {
    wrapper.setAttribute( 'curtain', 'true' )
    root.removeAttribute( 'fullLayout' )
    root.setAttribute( 'dialogLayout', true )
    navbar.getRoot().setAttribute( 'hidden', 'true' )
    content.removeAttribute( 'hidden' )
    loader.setAttribute( 'hidden', 'true' )
  }

  function disableDialogLayout (content)  {
    loader.removeAttribute( 'hidden' )
    content.setAttribute( 'hidden', 'true' )
  }


  // Handle the CSV Data
  // csvLoader.setCallback( (data) => {

  //     // UPLOAD CSV
  //     const reports = deliveryFormatter.formatDeliveryObjects( dataFormatter.csvToObjects( data, true ) )
  //     // reports.forEach( (report) => {
  //     //   service.createReport( {
  //     //     data: report,
  //     //     callback: (res) => console.log(res)
  //     //   } )
  //     // })
  //     console.log(reports)
  //   }
  // )


} )