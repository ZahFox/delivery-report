YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "AJAX",
        "Details",
        "List",
        "Login",
        "Navbar",
        "ReporterService",
        "Router"
    ],
    "modules": [
        "AJAX",
        "DeliveryReporter"
    ],
    "allModules": [
        {
            "displayName": "AJAX",
            "name": "AJAX",
            "description": "This module is used for sending JavaScript HTTP requests from a Web browser"
        },
        {
            "displayName": "DeliveryReporter",
            "name": "DeliveryReporter",
            "description": "This is the main module for the Delivery Reporter front-end Web application"
        }
    ],
    "elements": []
} };
});