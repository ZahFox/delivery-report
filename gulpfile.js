var gulp = require('gulp'),
sass = require('gulp-sass'),
babel = require('gulp-babel'),
minifycss = require('gulp-minify-css'),
uglify = require('gulp-uglify'),
usemin = require('gulp-usemin'),
htmlmin = require('gulp-htmlmin'),
imagemin = require('gulp-imagemin'),
rename = require('gulp-rename'),
concat = require('gulp-concat'),
notify = require('gulp-notify'),
cache = require('gulp-cache'),
changed = require('gulp-changed'),
rev = require('gulp-rev'),
browserSync = require('browser-sync'),
del = require('del');


// Compile SASS into CSS
gulp.task('sass', function(){
  return gulp.src('src/scss/styles.scss')
    .pipe(sass()) // Using gulp-sass
    .pipe(gulp.dest('src/css'))
});


// Clean
gulp.task('clean', function() {
  return del(['dist']);
});

// Default task
gulp.task('default', ['clean'], function() {
  gulp.start('sass', 'usemin', 'imagemin','copyfonts');
});

gulp.task('usemin', ['sass'], function () {
  return gulp.src('./src/**/*.html')
    .pipe(usemin({

      css: [ minifycss(),rev() ],

      html: [ htmlmin( {collapseWhitespace: true} ) ],

      js: [ 
        function () {
        return gulp.src('src/**/*.js')
          .pipe(babel())}, uglify(), rev()]

    }))
    .pipe(gulp.dest('dist/'));
});

// Images
gulp.task('imagemin', function() {
  return del(['dist/images']), gulp.src('src/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/images'))
    .pipe(notify({ message: 'Images task complete' }));
  });

gulp.task('copyfonts', ['clean'], function() {
  gulp.src('./src/fonts/**/*.{otf,ttf,woff,eof,svg}*')
    .pipe(gulp.dest('./dist/fonts'));
  });

// Watch
gulp.task('watch', ['browser-sync'], function() {
  // Watch .js files
  gulp.watch('{src/**/*.js,src/**/*.html}', ['usemin']);

  // Watch .scss files
  gulp.watch('src/scss/**/*', ['sass', 'usemin']);

    // Watch image files
  gulp.watch('src/images/**/*', ['imagemin']);

});

gulp.task('browser-sync', ['default'], function () {
var files = [
  'src/**/*.html',
  'src/**/*.css',
  'src/images/**/*.png',
  'src/**/*.js',
  'dist/**/*'
];

browserSync.init(files, {
  server: {
     baseDir: "dist",
     index: "index.html"
  }
});
    // Watch any files in dist/, reload on change
gulp.watch(['dist/**']).on('change', browserSync.reload);
});